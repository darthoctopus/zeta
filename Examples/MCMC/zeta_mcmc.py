#!/usr/bin/env python3
# Joel Ong <joel.ong@yale.edu>, 2022
'''
Constraining (q, ΔΠ, ε_g) generatively from mixed modes

We generate mixed-mode frequencies from a supplied set of p-mode
frequencies and guesses for q, ΔΠ, and ε_g, and use that to
compute a log-probability (= log[likelihood * prior]) function.
This in turn yields posterior distributions on the coupling parameters
via MCMC. We use this in the hare-and-hounds exercise of Ong & Gehan 2022
to estimate q independently of the analytic expressions (and in particular
to estimate systematic errors in determining q, ΔΠ, and ultimately ζ).
'''

from multiprocessing import Pool
import numpy as np
import matplotlib.pyplot as plt
import emcee
from zeta import couple, ζ_p
from mesa_tricks.utils.selection import merge_closest

def φ(q, ΔΠ, ε_g):
    '''
    Coordinate transformations:
    Since ε_g is cyclical, we consider it to describe an angular coordinate
    on a plane where the radial coordinate is given by ΔΠ.
    Sampling on this plane yields points lying within an annulus, which is easier
    to deal with (via an inverse transformation) than trying to cajole
    emcee into dealing with periodic boundary conditions otherwise.
    '''
    θ = 2 * np.pi * ε_g
    return (np.log10(q), ΔΠ * np.cos(θ), ΔΠ * np.sin(θ))

def φinv(ω, ξ, η):
    '''
    Inverse transformation, translating from the sampling plane back into
    physical quantities
    '''
    q = np.power(10, ω)
    ΔΠ = np.sqrt(ξ**2 + η**2)
    ε_g = np.arctan2(η, ξ) / (2 * np.pi)
    return q, ΔΠ, ε_g

class MCMCSession:
    '''
    MCMC session container to both keep track of immutable global properties
    associated with a given mode set (in particular ν_p, Δν, and a grid of n_g
    to use for generating mixed-mode frequencies)
    '''

    def __init__(self, ν_m, ν_p, ν_g, n_g_m, νmax=None, outname="test.h5",
                 ζ=None, ζ_threshold=None, e_ν=None):

        self.ν_m = ν_m
        self.n_g_m = n_g_m
        self.ν_p = ν_p
        self.ν_g = ν_g

        if νmax is None:
            self.νmax = np.mean(self.ν_p)
        else:
            self.νmax = νmax

        self.Δν = np.median(np.diff(self.ν_p))
        self.ΔΠ = np.median(-np.diff(1/self.ν_g))

        # we will limit attention to the 3 radial orders around νmax

        mask = (self.ν_m < (self.νmax + 3 * self.Δν)) & (self.ν_m > (self.νmax - 3 * self.Δν))

        if ζ_threshold is not None and ζ is not None:
            mask = mask & ((1-ζ) >= ζ_threshold)

        self.ν_m = self.ν_m[mask]
        self.n_g_m = self.n_g_m[mask]

        if e_ν is None:
            self.e_ν = 0.01 * np.ones_like(self.ν_m) # μHz: default value
        else:
            self.e_ν = e_ν[mask]

        # need to generate a few more radial orders of g-modes to ensure completeness

        self.n_g = np.arange(np.min(self.n_g_m)-4, np.max(self.n_g_m) + 4)[::-1]

        self.sampler = None
        self.backend = emcee.backends.HDFBackend(outname)

    def generate_freqs(self, q, ΔΠ, ε_g, trace=False):
        '''
        Generate a trial set of mixed-mode frequencies given (q, ΔΠ, ε_g)
        and the immutable set of ν_p associated with the session
        '''
        ν_g = 1 / ( ΔΠ * (self.n_g + ε_g) )
        ν_m = couple(self.ν_p, ν_g, q, q)
        if not trace:
            return np.sort(np.concatenate(ν_m))

        ν_m = np.concatenate(ν_m)
        # m_master, m_slave = merge_closest(self.ν_m, ν_m)
        plt.plot(((1/ν_m) % ΔΠ) * 1e6, ν_m, '.')
        plt.plot(((1/self.ν_m) % ΔΠ) * 1e6, self.ν_m, 'o', markerfacecolor='none')

        plt.xlim(0, ΔΠ * 1e6)
        plt.xlabel(r"$\Delta\Pi_l$/s")
        plt.ylabel(r"$\nu/\mu$Hz")

        return None

    def log_prob(self, x):
        '''
        Log (likelihood * prior) for use with emcee, taking in a tuple of
        coordinates on the sampling plane
        '''
        ω, ξ, η = x

        q, ΔΠ, ε_g = φinv(ω, ξ, η)

        if q > .5 or q < 1e-4:
            return -np.inf

        ν_trial = self.generate_freqs(q, ΔΠ, ε_g)
        m_master, m_slave = merge_closest(self.ν_m, ν_trial)

        if not np.all(m_master):
            return -np.inf

        if np.any(np.abs(1/self.ν_m[m_master] - 1/ν_trial[m_slave]) > ΔΠ):
            return -np.inf

        ζ = ζ_p(ν_trial[m_slave], q, ΔΠ, self.Δν, self.ν_p)

        return -np.sum((self.ν_m[m_master] - ν_trial[m_slave])**2 / 2
                        / self.e_ν[m_master]**2 / ζ**2)

    def __call__(self, nchain=1000, nwalkers=100, nthreads=6, reset=False, σ_ΔΠ=1):

        # Standardised initial configuration of walkers, uniformly
        # sampling the prior distribution

        p0_natural = np.array([
                np.random.permutation(np.power(10, np.linspace(-4, np.log10(0.5), nwalkers))),
                self.ΔΠ + np.random.normal(size=nwalkers) * 1e-6 * σ_ΔΠ,
                np.random.permutation(np.linspace(-.5, .5, nwalkers))
            ])

        # This is set by the data
        ndim = len(p0_natural)

        try:
            i = self.backend.iteration
        except (AttributeError, FileNotFoundError, OSError):
            resume = False
        else:
            resume = bool(i)

        p0 = np.array(φ(*p0_natural)).T

        if reset:
            self.backend.reset(nwalkers, ndim)

        with Pool(nthreads) as pool:
            sampler = emcee.EnsembleSampler(nwalkers, ndim,
                            log_prob_fn=self.log_prob, pool=pool, backend=self.backend)
            sampler.run_mcmc(p0 if (reset or not resume) else None, nchain, progress=True)

        self.sampler = sampler

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Compute Posterior Distributions for q, ΔΠ, ε_g")

    parser.add_argument('fname', type=str, help='Filename of .npy file containing eigensystem')
    parser.add_argument('--nwalkers', type=int, default=100, help='Number of walkers')
    parser.add_argument('--nchain', type=int, default=1000,
                        help='Number of samples to append to Markov chain')
    parser.add_argument('--nthreads', type=int, default=6, help='Number of threads to use')
    parser.add_argument('--threshold', default=None,
                        help='Threshold for 1-ζ for inclusion in set of modes used '
                        +'in the MCMC fit (0 means all modes, 1 means only p-modes)')

    args = parser.parse_args()

    fname = args.fname.replace('.npy', '')
    mat = np.load(f"{fname}.npy", allow_pickle=True)[()]

    ν_m = mat['modes']['ν']
    ν_p = mat['π']['ν']
    ν_g = mat['γ']['ν']
    n_g = mat['modes']['n_g']

    ζ = 1 - np.exp(np.interp(ν_m, ν_p, np.log(mat['π']['E']))) / mat['modes']['E']

    session = MCMCSession(ν_m, ν_p, ν_g, n_g, outname=f"{fname}.h5",
                          ζ=ζ, ζ_threshold=args.threshold)
    session(nchain=args.nchain, nwalkers=args.nwalkers, nthreads=args.nthreads)
