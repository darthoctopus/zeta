# zeta

Asymptotic mode coupling and ζ-function evaluation for making stretched echelle diagrams.

## Installation

I recommend just running `setup.py develop --user` (or with respect to the appropriate libdir) on the Git master, since I have no plans to put this on PyPI for the moment.

## Usage

See the examples folder.

## License
MIT License for now.
