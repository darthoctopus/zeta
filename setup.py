from setuptools import setup, find_packages
import os

version = '0a'

if __name__ == '__main__':
    setup(name='zeta',
          version=version,
          author='Joel Ong',
          author_email='joel.ong@yale.edu',
          description='Some routine for JWKB mode coupling and stretched echelle diagrams',
          license='MIT',
          url='https://gitlab.com/darthoctopus/zeta.git',
          packages=find_packages(),
          install_requires=['numpy', 'scipy']
          )
