#!/usr/bin/env python3
# Joel Ong <joelong@hawaii.edu>, 2022

'''
Mode Coupling using the JWKB Eigenvalue Equation

Eigenvalues are found by solving for the roots of the
characteristic function

F(ν) = tan Θ_p tan Θ_g - q = 0,

where Θ_p ~ π (ν - ν_p)/Δν and Θ_g ~ π (1/ν_g - 1/ν)/ΔΠ.

Note that the appearance of Θ_g in this characteristic equation
differs from the usual literature expressions by π/2; Lindsay et al.
(2022) demonstrate that this is necessary for agreement with
numerical mixed-mode frequency calculations in the limit q → 0,
and we reiterate the argument for it in Ong & Gehan 2023.
'''

import numpy as np
from scipy.integrate import solve_ivp

from .definitions import *
from .coupling import *

from .utils import *
