#!/usr/bin/env python3
# Joel Ong <joelong@hawaii.edu>, 2022

'''
Stretched period echelle power plot for mixed modes satisfying
the asymptotic eigenvalue equation

F(ν) = tan Θ_p tan Θ_g - q = 0,

where Θ_p ~ π (ν - ν_p)/Δν and Θ_g ~ π (1/ν_g - 1/ν)/ΔΠ.
'''

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline
from scipy.ndimage import gaussian_filter1d

from .coupling import make_τ
__all__ = ['period_echelle_power_plot']

def period_echelle_power_plot(f, power, ν_p, ΔΠ, q,
                              N_samp=1000, N_blur=2, ax=None, transpose=False,
                              inter_kwargs=None, x0=0, x1=1, **kwargs):

    '''
    Generate a period-echelle power plot given a list of p-mode frequencies,
    ΔΠ, and q.

    Inputs:

    f: frequencies at which the PSD is sampled
    power: PSD (works best with l = 0,2 already divided out)

    ν_p: p-mode frequencies
    ΔΠ: dipole-mode period spacing (in units such that ν * ΔΠ is dimensionless for each ν in ν_p)
    q: coupling strength
    '''

    if inter_kwargs is None:
        inter_kwargs = {}

    if ax is None:
        ax = plt

    inter_kwargs = {**dict(ext=1, s=0), **inter_kwargs}

    Δν = np.median(np.diff(ν_p))
    τ_ = make_τ(q, ν_p, Δν, ΔΠ, principal=False)

    τ = τ_(f)
    τinv_ = UnivariateSpline(τ[::-1], f[::-1], s=0)

    kwargs = {**dict(antialiased=True, cmap='Reds'), **kwargs}
    power_smooth = gaussian_filter1d(power, N_blur)
    p_ = UnivariateSpline(f, power, **inter_kwargs)

    P_min, P_max = np.min(τ), np.max(τ)
    N_orders = int((P_max - P_min) // ΔΠ + 1)

    j = np.floor(P_min / ΔΠ)
    w = np.linspace(x0, x1, N_samp) * ΔΠ
    x = w / ΔΠ
    y = τinv_(np.array([(i + j) * ΔΠ for i in range(N_orders)]))

    mesh = np.array([(i + j) * ΔΠ + w for i in range(N_orders)])

#     plt.imshow(1/mesh, extent=(0, ΔΠ * 1e6, 1/(P_min + N_orders * ΔΠ), 1/P_min))
    if transpose:
        return ax.pcolormesh(y, x, p_(τinv_(mesh)).T, **kwargs)
    return ax.pcolormesh(x, y, p_(τinv_(mesh)), **kwargs)
