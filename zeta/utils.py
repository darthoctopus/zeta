#!/usr/bin/env python3
# Joel Ong <joelong@hawaii.edu>, 2022

'''
Utility functions for use with ζ-function operations
'''

import numpy as np

def α_to_q(α, Δν, ΔΠ, ν, weak=False):
	'''
	Relation between the off-diagonal elements of the coupling matrix
	α (same units as ω²) and the JWKB coupling coefficient q (dimensionless).

	For derivation of this expression see Ong & Gehan 2023.

	We expect units of Δν and ΔΠ such that Δν * ΔΠ is dimensionless.
	'''
	if not weak:
		return (np.sqrt((2 * np.pi**2)**2 + (α / (4 * ν**2))**2) - 2 * np.pi**2) / (Δν * ΔΠ)
	else:
		return (α / (8 * np.pi * ν**2))**2 / Δν / ΔΠ
