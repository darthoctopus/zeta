#!/usr/bin/env python3
# Joel Ong <joelong@hawaii.edu>, 2022

'''
Mode Coupling using the JWKB Eigenvalue Equation

Eigenvalues are found by solving for the roots of the
characteristic function

F(ν) = tan Θ_p tan Θ_g - q = 0,

where Θ_p ~ π (ν - ν_p)/Δν and Θ_g ~ π (1/ν_g - 1/ν)/ΔΠ.

Solutions are found using either Newton's method or Halley's method
(default). Also, the stretching function τ (for constructing stretched)
echelle diagrams) is implemented. Two methods are provided: one based on ODE-
solving by the prescription of Mosser+ 2015, and the other is the closed-
form analytic construction given by Ong & Gehan 2023.
'''

import numpy as np
from scipy.integrate import solve_ivp

from .definitions import *

__all__ = [
    'couple', 'quality_check', 'make_τ'
]

# Solving the characteristic eigenvalue equation, F(ν) = 0

def couple_naive(ν_p, ν_g, q_p, q_g, maxiter=50):
    '''
    Solutions of the characteristic equation by self-consistent
    algebraic iterations. Unfortunately the orbit of convergence
    is so large, particularly for the p-dominated modes, that this
    doesn't work very well (yields spurious repeated roots) at large q.
    '''

    Δν = np.median(np.diff(ν_p))
    ΔΠ = -np.median(np.diff(1/ν_g))

    νm_p = np.copy(ν_p)
    νm_g = np.copy(ν_g)

    for _ in range(maxiter):
        νm_p = ν_p + Δν/np.pi * np.arctan(q_p / np.tan(Θ_g(νm_p, ΔΠ, ν_g)))
        νm_g = 1/( 1/ν_g - ΔΠ/np.pi * np.arctan(q_g / np.tan(Θ_p(νm_g, Δν, ν_p))) )

    return νm_p, νm_g

def newton_iteration(x, y, yp, λ=1.):
    '''
    Newton's method:
    x_{n+1} = x_n - f(x_n) / f'(x_n).

    We introduce a prefactor of λ for numerical damping
    (i.e. to reduce the size of the orbit of convergence.)
    '''
    return x - λ * y / yp

def halley_iteration(x, y, yp, ypp, λ=1.):
    '''
    Halley's method (2nd order Householder):
    x_{n+1} = x_n = 2 f f' / (2 f'² - f f''),
    again with damping.
    '''
    return x - λ * 2 * y * yp / (2 * yp * yp - y * ypp)

def couple_newton(ν_p, ν_g, q_p, q_g, maxiter=50, λ=.1):
    '''
    Solve the characteristic equation with Newton's method.
    This converges faster than the naive iterative procedure,
    but still requires significant damping to handle particularly
    low-frequency or high-q p-dominated mixed modes.
    '''

    Δν = np.median(np.diff(ν_p))
    ΔΠ = -np.median(np.diff(1/ν_g))

    νm_p = np.copy(ν_p)
    νm_g = np.copy(ν_g)

    for _ in range(maxiter):
        νm_p = newton_iteration(νm_p,
                                F(νm_p, ν_p, ν_g, Δν, ΔΠ, q_p),
                                Fp(νm_p, ν_p, ν_g, Δν, ΔΠ), λ=λ)
        νm_g = newton_iteration(νm_g,
                                F(νm_g, ν_p, ν_g, Δν, ΔΠ, q_g),
                                Fp(νm_g, ν_p, ν_g, Δν, ΔΠ), λ=λ)

    return νm_p, νm_g

def couple_halley(ν_p, ν_g, q_p, q_g, maxiter=50, λ=.5):
    '''
    Solve the characteristic equation with Halley's method.
    This converges even faster than Newton's method and is capable
    of handling quite numerically difficult scenarios with not
    very much damping.
    '''
    Δν = np.median(np.diff(ν_p))
    ΔΠ = -np.median(np.diff(1/ν_g))

    νm_p = np.copy(ν_p)
    νm_g = np.copy(ν_g)

    for _ in range(maxiter):
        νm_p = halley_iteration(νm_p,
                                F(νm_p, ν_p, ν_g, Δν, ΔΠ, q_p),
                                Fp(νm_p, ν_p, ν_g, Δν, ΔΠ),
                                Fpp(νm_p, ν_p, ν_g, Δν, ΔΠ), λ=λ)
        νm_g = halley_iteration(νm_g,
                                F(νm_g, ν_p, ν_g, Δν, ΔΠ, q_g),
                                Fp(νm_g, ν_p, ν_g, Δν, ΔΠ),
                                Fpp(νm_g, ν_p, ν_g, Δν, ΔΠ), λ=λ)

    return νm_p, νm_g

couple = couple_halley

def quality_check(νm_p, νm_g, ν_p, ν_g):
    '''
    Quality check on final mixed modes.

    Returns true if no numerical issues: in particular the mixed
    modes should be no more than ν²ΔΠ or Δν (whichever smaller)
    away from the pure modes.
    '''

    Δν = np.median(np.diff(ν_g))
    ΔΠ = -np.median(np.diff(1/ν_g))
    dν_p = np.minimum(Δν, ν_p**2 * ΔΠ)
    dν_g = np.minimum(Δν, ν_g**2 * ΔΠ)

    return not (np.any(np.abs(νm_p - ν_p) > dν_p)
        or np.any(np.abs(νm_g - ν_g) > dν_g))

# Create the τ quantity required to construct "stretched"
# echelle diagrams by the prescription of Mosser+ 2015

def make_τ_ODE(q, Δν, ΔΠ, ν_p, ν_g, use_ζ_p=True, P0=0, **kwargs):
    '''
    Solves the ODE
    dτ/dP = 1/ζ, such that Δτ ~ ΔΠ for idealised mixed modes.

    Input:
    q, Δν, ΔΠ, ν_p, ν_g: parameters describing the ζ function
    use_ζ_p: whether to use the full function ζ or its approximation ζ_p.

    Output:
    τ: callable.
    '''

    if callable(q):
        q_ = q
    else:
        q_ = lambda _ : q

    N = Δν / (np.median(ν_p)**2 * ΔΠ)

    kwargs = {**dict(method='Radau',
              max_step=min(q_(np.median(ν_p)), 1/N)/2), **kwargs}

    def dτdP(p, τ):
        '''
        y = dτ/dP = 1/ζ_p
        '''

        if use_ζ_p:
            return np.array([1 / ζ_p(1/(p * ΔΠ), q_(1/(p * ΔΠ)), ΔΠ, Δν, ν_p)])
        else:
            return np.array([1 / ζ(1/(p * ΔΠ), q_(1/(p * ΔΠ)), ΔΠ, Δν, ν_p, ν_g)])

    sol = solve_ivp(dτdP, np.array([1/np.max(ν_g), 1/np.min(ν_g)]) / ΔΠ,
                    [P0/ΔΠ], dense_output=True, **kwargs)

    τ = lambda P: sol.sol(P / ΔΠ).reshape(P.shape) * ΔΠ
    return τ

def make_τ(q, ν_p, Δν, ΔΠ, *args, principal=True, **kwargs):
    '''
    Returns a callable function τ(ν) that yields ν_g for
    idealised mixed modes in closed form.

    Input:
    q, ν_p, Δν, ΔΠ: parameters describing the ζ function

    Output:
    τ: callable
    '''

    if not callable(q):
        q_ = lambda _: q
    else:
        q_ = q

    def τ0(ν):
        θ_p = Θ_p(ν, Δν, ν_p)
        return 1/ν + ΔΠ / np.pi * np.arctan2(q_(ν) * np.cos(θ_p), np.sin(θ_p))

    if principal:
        return τ0

    # τ0 as defined above is discontinuous because of restriction
    # to the principal values of the arctan function by arctan2.
    # If we want it to be a continuous function, we need to
    # subtract some integer multiple of ΔΠ, specifically where
    # Θ_p = 2πn - π/2 for integer n.

    def τ(ν):
        return τ0(ν) - 2 * ΔΠ * np.floor(Θ_p(ν, Δν, ν_p=ν_p) / 2 / np.pi + 1/4)

    return τ
