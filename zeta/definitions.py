#!/usr/bin/env python3
# Joel Ong <joelong@hawaii.edu>, 2022

'''
Definitions for quantities entering into the asymptotic coupling equation
for mixed modes,

F(ν) = tan Θ_p tan Θ_g - q = 0,

where Θ_p ~ π (ν - ν_p)/Δν and Θ_g ~ π (1/ν_g - 1/ν)/ΔΠ.

Note that the appearance of Θ_g in this characteristic equation
differs from the usual literature expressions by π/2; Lindsay et al.
(2022) demonstrate that this is necessary for agreement with
numerical mixed-mode frequency calculations in the limit q → 0,
and we reiterate the argument for it in Ong & Gehan 2023.
'''

import numpy as np

__all__ = [
    'Θ_p', 'Θ_g', 'ζ', 'ζ_p', 'ζ_g',
    'F', 'Fp', 'Fpp',
]

# Evaluating the phase functions and ζ

def nearest(ν, ν_target):
    '''
    Utility function: given 1d arrays ν and ν_target,
    return a 1d array with the same shape as ν, containing
    the nearest elements of ν_target to each element of ν.
    '''
    if len(np.array(ν_target).shape) == 0:
        return ν_target * (0 * ν + 1)
    if len(np.array(ν).shape) == 0:
        return ν_target[np.argmin(np.abs(ν_target - ν))]
    return ν_target[np.argmin(np.abs(ν[:, None] - ν_target[None, :]), axis=1)]

def Θ_p(ν, Δν, ν_p=None, l=1, ε_p=None):
    '''
    p-mode phase function Θ_p. Either provide a list of
    p-mode frequencies ν_p, or a callable phase function ε_p.
    '''
    if ν_p is None:
        return np.pi * (ν/Δν - l/2 - ε_p(ν))
    else:
        return np.pi * np.where(
            (ν <= np.max(ν_p)) & (ν >= np.min(ν_p)),
            np.interp(ν, ν_p, np.arange(len(ν_p))),
            (ν - nearest(ν, ν_p)) / Δν + np.round((nearest(ν,ν_p)-ν_p[0]) / Δν)
        )

def Θ_g(ν, ΔΠ, ν_g=None, ε_g=None):
    '''
    g-mode phase function Θ_g. Either provide a list of
    g-mode frequencies ν_g, or a callable phase function ε_g.
    '''
    if ν_g is None:
        return -np.pi * (1/ν/ΔΠ - ε_g(ν))
    else:
        return np.pi * np.where(
            (ν <= np.max(ν_g)) & (ν >= np.min(ν_g)),
            -np.interp(1/ν, np.sort(1/ν_g), np.arange(len(ν_g))),
            (1/nearest(ν, ν_g) - 1/ν) / ΔΠ
        )

def ζ(ν, q, ΔΠ, Δν, ν_p, ν_g):
    '''
    ζ, the approximate local mixing fraction.
    '''
    θ_p = Θ_p(ν, Δν, ν_p)
    θ_g = Θ_g(ν, ΔΠ, ν_g)
    return 1/(1 + ΔΠ / Δν * ν**2 / q * np.sin(θ_g)**2 / np.cos(θ_p)**2)

def ζ_p(ν, q, ΔΠ, Δν, ν_p):
    '''
    ζ as defined using only the p-mode phase function.
    Agrees with ζ only at the eigenvalues (i.e. roots of the characteristic
    equation F(ν) = 0).
    '''
    θ = Θ_p(ν, Δν, ν_p)
    return 1/(1 + ΔΠ / Δν * ν**2 / (q * np.cos(θ)**2 + np.sin(θ)**2/q))

def ζ_g(ν, q, ΔΠ, Δν, ν_g):
    '''
    ζ as defined using only the g-mode phase function.
    Agrees with ζ only at the eigenvalues (i.e. roots of the characteristic
    equation F(ν) = 0).
    '''
    θ = Θ_g(ν, ΔΠ, ν_g)
    return 1/(1 + ΔΠ / Δν * ν**2 * (q * np.cos(θ)**2 + np.sin(θ)**2/q))

# Setting up the characteristic function F

def F(ν, ν_p, ν_g, Δν, ΔΠ, q):
    '''
    Characteristic function F such that F(ν) = 0 yields eigenvalues.
    '''
    return np.tan(Θ_p(ν, Δν, ν_p)) * np.tan(Θ_g(ν, ΔΠ, ν_g)) - q

def Fp(ν, ν_p, ν_g, Δν, ΔΠ, qp=0):
    '''
    First derivative dF/dν. Required for some numerical methods.
    '''
    return (
        np.tan(Θ_g(ν, ΔΠ, ν_g)) / np.cos(Θ_p(ν, Δν, ν_p))**2 * np.pi / Δν
        + np.tan(Θ_p(ν, Δν, ν_p)) / np.cos(Θ_g(ν, ΔΠ, ν_g))**2 * np.pi / ΔΠ / ν**2
        - qp
    )

def Fpp(ν, ν_p, ν_g, Δν, ΔΠ, qpp=0):
    '''
    Second derivative d²F / dν². Required for some numerical methods.
    '''
    return (
        2 * F(ν, ν_p, ν_g, Δν, ΔΠ, 0) / np.cos(Θ_p(ν, Δν, ν_p))**2 * (np.pi / Δν)**2
        + 2 * F(ν, ν_p, ν_g, Δν, ΔΠ, 0) / np.cos(Θ_g(ν, ΔΠ, ν_g))**2 * (np.pi / ΔΠ / ν**2)**2
        - 2 * np.tan(Θ_p(ν, Δν, ν_p)) / np.cos(Θ_g(ν, ΔΠ, ν_g))**2 * np.pi / ΔΠ / ν**3
        + 2 / np.cos(Θ_p(ν, Δν, ν_p))**2 * np.pi / Δν / np.cos(Θ_g(ν, ΔΠ, ν_g))**2 * np.pi / ΔΠ / ν**2
        - qpp
    )
